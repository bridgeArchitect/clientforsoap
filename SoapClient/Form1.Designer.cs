﻿namespace SoapClient
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateButton = new System.Windows.Forms.Button();
            this.oldLoginBox = new System.Windows.Forms.TextBox();
            this.newLoginBox = new System.Windows.Forms.TextBox();
            this.answerBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(12, 12);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(283, 48);
            this.updateButton.TabIndex = 0;
            this.updateButton.Text = "Update Record";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // oldLoginBox
            // 
            this.oldLoginBox.Location = new System.Drawing.Point(12, 66);
            this.oldLoginBox.Name = "oldLoginBox";
            this.oldLoginBox.Size = new System.Drawing.Size(283, 22);
            this.oldLoginBox.TabIndex = 1;
            // 
            // newLoginBox
            // 
            this.newLoginBox.Location = new System.Drawing.Point(310, 66);
            this.newLoginBox.Name = "newLoginBox";
            this.newLoginBox.Size = new System.Drawing.Size(289, 22);
            this.newLoginBox.TabIndex = 2;
            // 
            // answerBox
            // 
            this.answerBox.Enabled = false;
            this.answerBox.Location = new System.Drawing.Point(12, 94);
            this.answerBox.Multiline = true;
            this.answerBox.Name = "answerBox";
            this.answerBox.Size = new System.Drawing.Size(587, 94);
            this.answerBox.TabIndex = 3;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 201);
            this.Controls.Add(this.answerBox);
            this.Controls.Add(this.newLoginBox);
            this.Controls.Add(this.oldLoginBox);
            this.Controls.Add(this.updateButton);
            this.Name = "mainForm";
            this.Text = "Main Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.TextBox oldLoginBox;
        private System.Windows.Forms.TextBox newLoginBox;
        private System.Windows.Forms.TextBox answerBox;
    }
}

