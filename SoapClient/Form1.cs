﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SoapClient
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            /* create soap-client */
            using (ServiceReference1.SomeSoapServiceClient client = new ServiceReference1.SomeSoapServiceClient())
            {
                /* make requast */
                ServiceReference1.updateParameters req = new ServiceReference1.updateParameters();
                req.login = oldLoginBox.Text;
                req.nlogin = newLoginBox.Text;
                /* show answer */
                string answer = client.updateParameters(req).updateParametersResult.ToString();
                answerBox.Text = answer;
            }
        }

    }
}
